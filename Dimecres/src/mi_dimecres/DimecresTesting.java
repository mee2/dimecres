package mi_dimecres;

import static org.junit.Assert.*;

import org.junit.Test;

public class DimecresTesting {

	@Test
	public final void testDiesSetmana() {
		assertEquals(Dimecres.diaSetmana("03/03/2019"), "DIUMENGE");
		assertEquals(Dimecres.diaSetmana("29/02/1600"), "DIMARTS");
		assertEquals(Dimecres.diaSetmana("01/01/1600"), "DISSABTE");
		assertEquals(Dimecres.diaSetmana("31/12/2000"), "DIUMENGE");
		assertEquals(Dimecres.diaSetmana("29/02/2000"), "DIMARTS");
		assertEquals(Dimecres.diaSetmana("03/03/2003"), "DILLUNS");
		assertEquals(Dimecres.diaSetmana("07/03/2019"), "DIJOUS");
	}

	@Test
	public final void testDataValida() {
		assertEquals(true, Dimecres.dataValida("03/03/2019" ));
		assertEquals(false, Dimecres.dataValida("29/02/1601" ));
		assertEquals(false, Dimecres.dataValida("32/03/2019" ));
		assertEquals(true, Dimecres.dataValida("29/02/1600" ));
		assertEquals(false, Dimecres.dataValida("31/06/2018" ));
		assertEquals(true, Dimecres.dataValida("31/12/2019" ));
	}

}
